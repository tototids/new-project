import grails.util.GrailsNameUtils as GNU

class BootstrapTagLib {

    static namespace = "bs"

    def navbar = { attrs, body ->

        out << '<n:header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">\n'
        out << '  <n:div class="container">\n'

        def brand = attrs.remove('brand')
        if(brand) {
            out << '<n:div class="navbar-header">\n'
            out << '  <n:button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">\n'
            out << '    <n:span class="sr-only">Toggle navigation</n:span>\n'
            out << '    <n:span class="icon-bar"></n:span>\n'
            out << '    <n:span class="icon-bar"></n:span>\n'
            out << '    <n:span class="icon-bar"></n:span>\n'
            out << '  </n:button>\n'
            out << '  <n:a href="index.zul" class="navbar-brand">' + brand + '</n:a>\n'
            out << '</n:div>\n'
        }

        out << '    <n:nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">\n'

        def left = attrs.remove('left')
        if(left) {
            out << '<n:ul class="nav navbar-nav">\n'
            left.each {k, v ->
                out << '<n:li>\n'
                out << ' <n:a href="' + k +'.zul">' + v + '</n:a>\n'
                out << '</n:li>\n'
            }
            out << '</n:ul>\n'
        }

        def right = attrs.remove('right')
        if(right) {
            out << '<n:ul class="nav navbar-nav navbar-right">\n'
            right.each {k, v ->
                out << '<n:li>\n'
                if(k.startsWith('_')) {
                    k = k - '_'
                    out << '<a id="' + k + '">' + v + '</a>\n'
                }
                else {
                    out << '<n:a href="#' + k +'">' + v + '</n:a>\n'
                }
                out << '</n:li>\n'
            }
            out << '</n:ul>\n'
        }

        if(body) {
            out << body()
        }

        out << '    </n:nav>\n'
        out << '  </n:div>\n'
        out << '</n:header>\n'
    }

    /**
     *    <bs:navbox align="right"
     *      apply="bootstrap.LoginComposer"
     *      items="[
     *        textbox:"User name",
     *        password:"Password",
     *        button:"Sign in"
     *    ]" />
     *
     */

     /*
     *    <bs:navbox align="right"
     *      apply="bootstrap.LoginComposer"
     *      items="[
     *        textbox:  [username: "User name"],
     *        password: [password: "Password"],
     *        button:   [signin:   "Sign in"]
     *      ]"/>
     *
    **/
    private tag(String key) {
        switch(key) {
            case "password": return "textbox"
            default:
                return key
        }
    }

    private specialAttr(String key, strOrMap) {
        def v = strOrMap instanceof Map ? strOrMap.iterator()[0].value : strOrMap
        switch(key) {
            case "password": return "type=\"password\" placeholder=\"$v\" class=\"form-control\""
            case "textbox":  return "placeholder=\"$v\" class=\"form-control\""
            case "button":   return "label=\"$v\" class=\"btn-success\""
        }
    }

    private prefix(String tag) {
        switch(tag) {
            case "textbox": return "txt"
            case "button":  return "btn"
            default:
                return ''
        }
    }

    private id(String pf, strOrMap) {
        if(strOrMap instanceof Map) {
            return strOrMap.iterator()[0].key
        }
        def str = strOrMap
        return GNU.getPropertyNameRepresentation(prefix(pf) + " " + str)
    }

    def navbox = { attrs, body ->
        def align = attrs.remove('align')
        if(align) {
            align = "navbar-${align}"
        } else {
            align = ""
        }

        def items = attrs.remove('items')
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<div $a>\n"
        out << "  <n:form class=\"navbar-form $align\">\n"

        if(items) {
            items.each { k, v ->
                def tag = tag(k)
                def id  = id(tag, v)
                def sAttr = specialAttr(k, v)
                out << '<n:div class="form-group">\n'
                out << "  <$tag id=\"$id\" $sAttr />\n"
                out << '</n:div>\n'
            }
        }
        out << '  </n:form>\n'
        out << '</div>\n'
    }

    def header = { attrs, body ->
        def title = attrs.remove('title')
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="bs-header ' + cls + '"'
        } else {
            cls = 'class="bs-header"'
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<n:div $cls $a>\n"
        out << '  <n:div class="container">\n'
        out << "    <n:h1>${title}</n:h1>\n"
        out << '    <n:p>\n'
        if(body) {
            out << body()
        }
        out << '    </n:p>\n'
        out << '  </n:div>\n'
        out << '</n:div>\n'
    }

    def container = { attrs, body ->
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="container bs-docs-container ' + cls + '"'
        } else {
            cls = 'class="container bs-docs-container"'
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<n:div $cls $a>\n"
        if(body) {
            out << body()
        }
        out << '</n:div>\n'
    }

    def row = { attrs, body ->
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="row ' + cls + '"'
        } else {
            cls = 'class="row"'
        }

        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<n:div $cls $a>\n"
        if(body) {
            out << body()
        }
        out << '</n:div>\n'
    }

    def column = { attrs, body ->
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="' + cls + '"'
        } else {
            cls = ''
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<n:div $cls $a>\n"
        if(body) {
            out << body()
        }
        out << '</n:div>\n'
    }

    def sidebar = { attrs, body ->
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="bs-sidebar hidden-print ' + cls + '"'
        } else {
            cls = 'class="bs-sidebar hidden-print"'
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<n:div $cls $a>\n"
        if(body) {
            out << body()
        }
        out << '</n:div>\n'
    }

    /**
    * TODO: model can be a tree
    **/
    def sidenav = { attrs, body ->
        def caption = attrs.remove('caption')
        def items = attrs.remove('model')
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="nav bs-sidenav ' + cls + '"'
        } else {
            cls = 'class="nav bs-sidenav"'
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")

        out << "<n:ul $cls $a>"
        out << "<n:li>"
        if(caption instanceof Map) {
            def e = caption.iterator()[0]
            out << "<n:a href=\"#${e.key}\">${e.value}</n:a>"
        } else {
            out << "<n:a>${caption}</n:a>"
        }
        if(items) {
            out << '<n:ul class="nav">'
            items.each { k,v ->
                out << '<n:li>'
                out << "  <n:a href=\"#${k}\">${v}</n:a>"
                out << '</n:li>'
            }
            out << "</n:ul>"
        }
        out << "</n:li>"
        out << "</n:ul>"

    }

    def section = { attrs, body ->
        def cls = attrs.remove('class')
        def tag = "n:div"
        if(attrs['apply']) {
            tag = "div"
        }
        if(cls) {
            cls = 'class="bs-docs-section ' + cls + '"'
        } else {
            cls = 'class="bs-docs-section"'
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<$tag $cls $a>"
        if(body) {
            out << body()
        }
        out << "</$tag>"
    }

    def pageHeader = { attrs, body ->
        def id = attrs.remove('id')
        def cls = attrs.remove('class')
        if(cls) {
            cls = 'class="page-header ' + cls + '"'
        } else {
            cls = 'class="page-header"'
        }
        def a = attrs.collect { k, v -> "$k=\"$v\"" }.join(" ")
        out << "<n:div $cls $a>"
        if(body) {
            out << "<n:h1 id=\"$id\">"
            out << body()
            out << "</n:h1>"
        }
        out << '</n:div>'
    }

    def collapsible = {

    }

    def form = { attrs, body ->

    }

}
