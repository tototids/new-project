class ZkBootstrapTagLib {

    static namespace = "zk"

    def bootstrap = { attrs, body ->
        def excludes = attrs.remove('excludes')
        (['bootstrap','bootstrap-theme'] - excludes).each { css ->
            def bootstrapCss = z.resource(dir:'bootstrap/css', file:"${css}.css")
            out << "<?link rel=\"stylesheet\" type=\"text/css\" href=\"${bootstrapCss}\" ?>\n"
        }

        def bootstrapJs = z.resource(dir:'bootstrap/js', file:'bootstrap.js')
        out << "<?script type=\"text/javascript\" src=\"${bootstrapJs}\" ?>\n"
        ["respond.min", "html5shiv"].each { js ->
            def jsFile = z.resource(dir:'bootstrap/js', file:"${js}.js")
            out << "<?script src=\"${jsFile}\" if=\"\${zk.ie < 9}\" ?>\n"
        }

        /*
        (['zul_wgt','zul_sel','zul_menu']-excludes).each { js ->
            def script = z.resource(dir:'bootstrap/zk', file:"${js}.js")
            out << "<?script type=\"text/javascript\" src=\"${script}\" ?>"
        }
        */

    }

    def grails = { attrs, body ->
        out << '<zk xmlns:w="client" xmlns:n="native" xmlns:h="http://www.w3.org/1999/xhtml">'
        if(body) {
            out << body()
        }
        out << '</zk>'
    }

}