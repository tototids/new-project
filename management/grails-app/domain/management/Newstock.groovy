package management

class Newstock {
	
    String barcode
	String nameproduct
	String modeproduct
	Double pricetoon
	int countproduct
	String nameunit
	Date productimport
	Date productexport

    static constraints = {
    	barcode unique: true
    }
}
