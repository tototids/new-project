package management
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.lang.reflect.Method;
import java.lang.reflect.Field;

class LoginComposer extends zk.grails.Composer {

    def afterCompose = { window ->
        $('#user').focus()
        Date date = new Date()      
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat() 
		df.applyPattern("HHmmss")

    	$('#clear').on('click',{
    		$('#user,#pass,#status').val("")
    	})

    	$('#login').on('click',{
    		def check = Employee.findByEmid($('#user').text())
    		if(check != null){
    			if(check.emid == $('#user').text() && check.passwords == $('#pass').text()){
    				session.user = check.emid
    				session.groupname = check.groupname
    				def savestamp = new Stamplogin()
    				savestamp.user = session.user
					savestamp.groupname = session.groupname
					savestamp.timelogin = df.format(date)
					savestamp.save()
    				redirect(uri: "/corder.zul")
    			}
    			else{
    				$('#status').val("รหัสผ่านไม่ถูกต้องค่ะ")
    			}
    		}else{
    			if($('#user').text() == "admin" && $('#pass').text() == "admin"){
    				session.user = "admin"
	    			session.groupname = "admin"

	    			def savestamp = new Stamplogin()
	    			savestamp.user = session.user
					savestamp.groupname = session.groupname
					savestamp.timelogin = df.format(date)
					savestamp.save()

					def groupsave = new Groupemployee()
		        	groupsave.namegroup = session.groupname
		        	groupsave.k1 = "เพิ่มสินค้า"
		        	groupsave.k2 = "สต๊อกสินค้า"
		        	groupsave.k3 = "พนักงาน"
		        	groupsave.k4 = "ยอดขาย"
		        	groupsave.k5 = "รับลูกค้า"
                    groupsave.k6 = "ข้อมูลร้าน"
                    groupsave.k7 = "คิดเงิน"
		        	groupsave.save()
	    			redirect(uri: "/corder.zul")
    			}
                else{
                    $('#status').val("ไม่พบ Username "+$('#user').text()+" ครับ")
                    $('#user').val("")
                    $('#pass').val("")
                }
    		}
    	})
    }
}
