package management
import java.lang.*

class SumsellComposer extends zk.grails.Composer { 

List<String> billidarray = new ArrayList<String>()
    Boolean checkprint = false
    Double sum = 0
    Double sumall = 0

    def afterCompose = { window ->

        billidarray.add("begin")

        //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)

        if(checkgroup != null){
            if(checkgroup.k4 == session.k4){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================

        String stringstate = ""
        for(Final m : Final.findAll()){
            if(stringstate != m.date.substring(0,10)){
                stringstate = m.date.substring(0,10)

                $('#search').append{
                    comboitem(label:m.date.substring(0,10))
                }
            }
        }

        $('#search').on('select',{
            printrow()
        })
         
    }

    public void printrow(){
        if(checkprint == true){
            $('#listbuy > rows > row').detach()
            sumall = 0
            billidarray.clear()
             
        }
        for(Final b : Final.findAll()){
            if($('#search').text() == b.date.substring(0,10)){ 
                    $('#listbuy > rows').append{
                        row(style:"font-weight:bold;color:black;background:pink;"){
                            label(value:b.date.substring(0,10),style:"font-weight:bold;color:black")
                            label(value:b.tabl,style:"font-weight:bold;color:black")
                            label(value:b.nameproduct,style:"font-weight:bold;color:black")
                            label(value:b.priceperunit,style:"font-weight:bold;color:black")
                            label(value:b.cus,style:"font-weight:bold;color:black")
                            label(value:b.usersale,style:"font-weight:bold;color:black")
                            label(value:b.sum,style:"font-weight:bold;color:black")
                            sumall = sumall + b.sum
                            label(value:"บาท",style:"font-weight:bold;color:black")
                        }
                    }
                    checkprint = true
                }
                
            }
            $('#listbuy > rows').append{
            sumall = twodigit(sumall)
            row(spans:"6",style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
                label(value:"รวมทั้งหมดของวัน",style:"font-size:14px;font-weight:bold;color:red")
                label(value:"${sumall}",style:"font-size:14px;font-weight:bold;color:red")
                label(value:"บาท",style:"font-size:14px;font-weight:bold;color:red")
            }
            checkprint = true
        }
    }

    public BigDecimal twodigit(Double num){
        BigDecimal twodi = new BigDecimal(num)
        twodi = twodi.setScale(2,BigDecimal.ROUND_HALF_EVEN)
        return twodi
    } 

}