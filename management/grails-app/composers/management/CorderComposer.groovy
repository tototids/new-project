package management
import java.util.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

import java.io.FileOutputStream

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;


class CorderComposer extends zk.grails.Composer {    

    Double tempsumall = 0
    boolean checkprin = false

    def afterCompose = { window ->

        $('#idproduct').focus()

        int countpro = Integer.parseInt($('#count').text())
        int table = Integer.parseInt($('#table').text())
        int customer = Integer.parseInt($('#customers').text())

        Date date = new Date()
        $('#imday').val(date)

        $('#count').on('click',{
            $('#count').select()
        })

         $('#table').on('click',{
            $('#table').select()
        })

          $('#customers').on('click',{
            $('#customers').select()
        })

        $('#count').on('change',{
            countpro = Integer.parseInt($('#count').text())
        })

        $('#table').on('change',{
            table = Integer.parseInt($('#table').text())
        })

        $('#customers').on('change',{
            customer = Integer.parseInt($('#customers').text())
        })


        def findem = Employee.findByEmid(session.user)
        if(findem != null){
            $('#idem').val(findem.emid)
            $('#nameem').val(findem.nameem)
        }

printrowsell()

        $('#idproduct').on('change',{
            Double sumtemp = 0
            Double sumall = 0
            def findidbar = Newadd.findByBarcode($('#idproduct').text())

            if(findidbar != null){
                String bar = findidbar.barcode
                String name = findidbar.nameproduct
                String unit = findidbar.nameunit
                Double priceu = findidbar.pricetoon
                sumtemp = countpro*findidbar.pricetoon

                boolean checkseartemp = false
                for(Corder findtempbar : Corder.findAll()){
                    if(findtempbar.barcode == $('#idproduct').text() && session.user == findtempbar.emid){
                        findtempbar.countpro = countpro+findtempbar.countpro
                        findtempbar.priceperunit = priceu
                        findtempbar.sum = sumtemp+findtempbar.sum
                        findtempbar.save()

                        printrowsell()
                        $('#idproduct').val("")
                        $('#idproduct').focus()
                        $('#count').val(1)
                        countpro = 1
                        checkseartemp = true

                    }
                }
                if(checkseartemp == false){
                    def savetemp = new Corder()
                    savetemp.emid = session.user
                    savetemp.barcode = bar
                    savetemp.table = table
                    savetemp.nameproduct = name
                    savetemp.countpro = countpro
                    savetemp.nameunit = unit
                    savetemp.priceperunit = priceu
                    savetemp.sum = sumtemp
                    savetemp.save()

                    printrowsell()
                    $('#idproduct').val("")
                    $('#idproduct').focus()
                    $('#count').val(1)
                    countpro = 1
                    checkseartemp = true
                   
                }
           }
          else{
                printrowsell()
                $('#count').val(1)
                countpro = 1
           }

        }) 

 $('#save').on('click',{ 

         if($('#table').text() == "0" ||
                $('#customers').text() == "0"||
                $('#count').text() == "0"
                ){
                alert("กรุณากรอกข้อมูลให้ครบด้วยครับ")
            }else{

         for(Corder temp : Corder.findAll()){
                if(temp.emid == session.user){
                    def savebill = new Listbill()
                    savebill.usersale = session.user
                    savebill.date = dateandtime()
                    savebill.cus = $('#nameem').text()
                    savebill.tabl = temp.table
                    savebill.emid = temp.emid
                    savebill.barcode = temp.barcode
                    savebill.nameproduct = temp.nameproduct
                    savebill.nameunit = temp.nameunit
                    savebill.countpro = temp.countpro
                    savebill.priceperunit = temp.priceperunit
                    savebill.sum = temp.sum
                    savebill.save()
                  
                }
            }

            for(Corder temp : Corder.findAll()){
                if(session.user == temp.emid){
                    temp.delete()
                }
            }
        $('#print').setHref("static/ext/pdf/"+dateandtimeforbill()+".pdf");
        $('#print').setTarget("_new");
        createpdf();
        alert("บันทึกข้อมูลเรียบร้อยกรุณากดพิมพ์")
    }
}) 

    $('#print').on('click',{
        def del = Stamplogin.findByUser(session.user)
                        if(del != null){
                            session.user = ""
                            session.groupname = ""
                            session.k1 = ""
                            session.k2 = ""
                            session.k3 = ""
                            session.k4 = ""
                            session.k5 = ""
                            session.k6 = ""
                            session.k7 = ""
                            del.delete()
                            redirect(uri: "/login.zul")
                        }
    }) 
}

public String dateandtime(){
        Date date = new Date()
        java.text.SimpleDateFormat df= new java.text.SimpleDateFormat();
         df.applyPattern("dd/MM/yyyy HH:mm:ss")
        return df.format(date)

    }

public String dateandtimeforbill(){
        Date date = new Date()
        java.text.SimpleDateFormat df= new java.text.SimpleDateFormat();
        df.applyPattern("dd-MM-yyyy-HH-mm-ss")
    
        return df.format(date)

    }

public String GetString(String str) {          
        return str
    } 



//เริ่ม

public void createpdf(){
    try {
        Rectangle pagesize = new Rectangle(210f, 297f)
        Document document = new Document(pagesize, 1.5f, 1.5f, 1.5f, 1.5f)
        PdfWriter.getInstance(document, new FileOutputStream(application.getRealPath("/ext/pdf/"+dateandtimeforbill()+".pdf")));
        document.open();
        addMetaData(document);
        addTitlePage(document);
        document.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
private void addMetaData(Document document) {
    document.addTitle("My first PDF");
    document.addSubject("Using iText");
    document.addKeywords("Java, PDF, iText");
    document.addAuthor("Lars Vogel");
    document.addCreator("Lars Vogel");
}



private void addTitlePage(Document document) throws DocumentException {
    BaseFont basefont;
    String compa
    basefont = BaseFont.createFont(application.getRealPath("ARIALUNI.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
    Font catFont = new Font(basefont, 12,Font.BOLD);
    Font redFont = new Font(basefont, 9,Font.NORMAL, BaseColor.RED);
    Font smallBold = new Font(basefont, 9,Font.BOLD);
    Font smallNomal = new Font(basefont, 9,Font.NORMAL);
    LineSeparator line = new LineSeparator();

    for(Infosetting checkdata : Infosetting.findAll()){
                if(checkdata != null){
                    compa = checkdata.namecompany
                }
            }

    Paragraph preface = new Paragraph();
    preface.add(new Paragraph(compa, catFont));
    preface.add(new Paragraph("วันที่ :"+ new Date(), smallBold));
    addEmptyLine(preface, 1);
    preface.add(new Paragraph("ชื่อพนักงาน :"+$('#nameem').text(), smallBold));
    preface.add(new Paragraph("โต๊ะ :"+$('#table').text(), smallBold));
    preface.add(new Paragraph("จำนวนลูกค้า :"+$('#customers').text(), smallBold));
    preface.add(line);
    addEmptyLine(preface, 1);
    Paragraph menu = new Paragraph("รายการที่สั่ง", smallBold);
    menu.setAlignment(Element.ALIGN_CENTER);
    preface.add(menu);
    addEmptyLine(preface, 1);
    PdfPTable table = new PdfPTable(2);
    PdfPCell c1 = new PdfPCell(new Phrase("รายการ", smallBold));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);
    c1 = new PdfPCell(new Phrase("จำนวน", smallBold));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    for(Corder temp : Corder.findAll()){
                if(session.user == temp.emid){
                    table.addCell(new Paragraph(temp.nameproduct, smallBold));
                    table.addCell(new Paragraph("${temp.countpro}"+" "+temp.nameunit, smallBold));
                }
            }

    document.add(preface);
    document.add(table);
    document.newPage();
}

private void addEmptyLine(Paragraph paragraph, int number) {
    for (int i = 0; i < number; i++) {
        paragraph.add(new Paragraph(" "));
    }
}
//จบ




public void printrowsell(){
        Double sumallm = 0
        int j = 1
        int checkbutton = 0
    
 if(checkprin == true){
            $('#listbuy > rows > row').detach()
            checkprin = false
        }

 for(Corder temp : Corder.findAll()){
            if(temp.emid == session.user){
                $('#listbuy > rows').append{
                row{
                    label(value:"${j}",style:"font-size:14px;font-weight:bold;color:black")
                    label(value:temp.nameproduct,style:"font-size:14px;font-weight:bold;color:black")
                    label(value:"${temp.countpro}",style:"font-size:14px;font-weight:bold;color:black")
                    label(value:"${temp.priceperunit}",style:"font-size:14px;font-weight:bold;color:black")
                    label(value:"${temp.sum}",style:"font-size:14px;font-weight:bold;color:black")
                    sumallm = sumallm+temp.sum
                    tempsumall = sumallm
                    button(id:temp.barcode,style:"font-size:14px;font-weight:bold;color:black",image:"/ext/images/icon/database-delete-icon.png",mold:"trendy")
                    }
                    j++
                    checkprin = true
                }
                $('#listbuy > rows > row > button').on('click',{
                    for(Corder finddelete : Corder.findAll()){
                        if(finddelete.barcode == it.target.id && finddelete.emid == session.user){
                            finddelete.delete()
                            redirect(uri: "/corder.zul")
                        }
                    }
                        
                })

            }
        }
    }      
}


