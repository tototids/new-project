package management


class CpassComposer extends zk.grails.Composer {

    def afterCompose = { window ->

        $('#cancel').on('click',{
            redirect(uri: "/corder.zul")
        })



        def findem = Employee.findByEmid(session.user)
        if(findem != null){
            $('#emid').val(findem.emid)
            $('#passem').val(findem.passwords)
            $('#nameem').val(findem.nameem)
            $('#numpeople').val(findem.numpeople)
            $('#listmode').val(findem.groupname)
            $('#address').val(findem.address)
            $('#tel').val(findem.tel)
        }


        $('#save').on('click',{
                if($('#address').text() == "" ||
                $('#tel').text() == ""||
                $('#newpass').text() == ""
                ){
                alert("กรุณากรอกข้อมูลให้ครบด้วยครับ")
            }
            else{
                def searchidem = Employee.findByEmid($('#emid').text())
                if(searchidem != null){
                    searchidem.emid = $('#emid').text()
                    searchidem.passwords = $('#passem').text()
                    searchidem.nameem = $('#nameem').text()
                    searchidem.numpeople = $('#numpeople').text()
                    searchidem.groupname = $('#listmode').text()
                    searchidem.address = $('#address').text()
                    searchidem.tel = $('#tel').text()
                    searchidem.passwords = $('#newpass').text()
                    searchidem.save()
                    alert("เปลี่ยนรหัสผ่านเรียบร้อย")
                    redirect(uri: "/corder.zul")
                }
                else{
                    def saveem = new Employee()
                    saveem.emid = $('#emid').text()
                    saveem.passwords = $('#passem').text()
                    saveem.nameem = $('#nameem').text()
                    saveem.numpeople = $('#numpeople').text()
                    saveem.groupname = $('#listmode').text()
                    saveem.address = $('#address').text()
                    saveem.tel = $('#tel').text()
                    saveem.passwords = $('#newpass').text()
                    saveem.save()
                    alert("เปลี่ยนรหัสผ่านเรียบร้อย")
                    redirect(uri: "/corder.zul")
                }
            }
        })

        

    }
}
