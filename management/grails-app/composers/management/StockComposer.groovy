package management


class StockComposer extends zk.grails.Composer {

    def afterCompose = { window ->
        //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k2 == session.k2){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================

        $('#namemodestock').focus()

    	int countlist = 1
    	for(Stock m : Stock.findAll()){
    		$('#listnamemode > rows').append{
    			row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
    				label(value:"${countlist}",style:"font-size:14px;font-weight:bold;color:black")
    				label(value:m.namemode,style:"font-size:14px;font-weight:bold;color:black")
    				button(id:m.namemode,style:"font-size:14px;font-weight:bold;color:black",image:"/ext/images/icon/delete_recycle_bin.png",mold:"trendy")
    			}
    		}
            countlist++
	    	$('#listnamemode > rows > row > button').on('click',{
	        	def delete = Stock.findByNamemode(it.target.id)
	        	delete.delete()
	        	redirect(uri: "/stock.zul")
	        })
    	}
    
        $d('#b').on('click'){ ev ->
            redirect(uri: "/newstock.zul")
        }
        
        $('#clear').on('click',{
        	$('#namemodestock').val("")
        })

        $('#submit').on('click',{
        	if($('#namemodestock').text() == ""){
        		alert("กรุณากรอกชื่อหมวดสินค้าด้วยครับ")
        	}
        	else{
        		Stock savemode = new Stock()
	        	savemode.namemode = $('#namemodestock').text()
	        	savemode.save()
	        	redirect(uri: "/stock.zul")
        	}
        })
    }
}
