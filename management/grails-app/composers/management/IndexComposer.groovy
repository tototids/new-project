package management


class IndexComposer extends zk.grails.Composer {

    def afterCompose = { window ->
       int count = 0
        boolean clickalert = true
        boolean onclick = false
        boolean checkprin = false
        def checksetting = Infosetting.findAll()
        if("${checksetting}" == "[]"){
            $d('#statesetting').append{
                label(value:"เพื่อให้โปรแกรมทำงานได้สมบูรณ์ กรุณา กรอกรายละเอียดเกี่ยวกับร้านของท่านก่อน")
                $('#pageclick').setVisible(true)
            }
        }
        else{
            $('#pageclick').setVisible(false)
        }


		$('#useronline').val(session.user)
		$('#groupuse').val(session.groupname)
		$('#logout').on('click',{
			def del = Stamplogin.findByUser(session.user)
			   	if(del != null){
			    	session.user = ""
			    	session.groupname = ""
			    	session.k1 = ""
				    session.k2 = ""
				    session.k3 = ""
				    session.k4 = ""
				    session.k5 = ""
				    session.k6 = ""
				    session.k7 = ""
			    	del.delete()
			    	redirect(uri: "/login.zul")
			    }
		})
			def checkgroup = Groupemployee.findByNamegroup(session.groupname)
			    if(checkgroup != null){
			        session.k1 = "เพิ่มสินค้า"
			        session.k2 = "สต๊อกสินค้า"
			        session.k3 = "พนักงาน"
			        session.k4 = "ยอดขาย"
			        session.k5 = "รับลูกค้า"
			        session.k6 = "ข้อมูลร้าน"
			        session.k7 = "คิดเงิน"
			    }
			    else{
			        redirect(uri: "/login.zul")
			    } 
    }
}
