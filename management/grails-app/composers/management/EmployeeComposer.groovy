package management


class EmployeeComposer extends zk.grails.Composer {

    def afterCompose = { window ->
       //=============================== check session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k3 == session.k3){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================

        $('#namegroup').focus()

    	int countlist = 1
    	for(Groupemployee g : Groupemployee.findAll()){
    		$('#listgroup > rows').append{
    			row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
    				label(value:"${countlist}",style:"font-size:12px;font-weight:bold;color:black")
    				label(value:g.namegroup,style:"font-size:12px;font-weight:bold;color:black")
    				label(value:g.k1,style:"font-size:12px;font-weight:bold;color:black")
    				label(value:g.k2,style:"font-size:12px;font-weight:bold;color:black")
    				label(value:g.k3,style:"font-size:12px;font-weight:bold;color:black")
    				label(value:g.k4,style:"font-size:12px;font-weight:bold;color:black")
    				label(value:g.k5,style:"font-size:12px;font-weight:bold;color:black")
                    label(value:g.k6,style:"font-size:12px;font-weight:bold;color:black")
                    label(value:g.k7,style:"font-size:12px;font-weight:bold;color:black")
    				button(id:g.id,orient:"vertical",mold:"trendy",image:"/ext/images/icon/delete_recycle_bin.png",style:"font-size:12px;font-weight:bold;color:black")
    			}
    		}
    		countlist++
    		$('#listgroup > rows > row > button').on('click',{
    			def groupdelete = Groupemployee.findById(it.target.id)
    			if(groupdelete != null){
					for(Employee moo : Employee.findAll()){
		
					if(moo.groupname == g.namegroup){
					moo.delete()
					}
				}
    				groupdelete.delete()
    				redirect(uri: "/employee.zul")
    			}
    		})
    	}
        
        $('#save').on('click',{
        	if($('#namegroup').text() == ""){
        		alert("กรุณาระบุชื่อกลุ่มด้วยค่ะ")
        	}
        	else if(!$('#k1').checked() &&
        			!$('#k2').checked() &&
        			!$('#k3').checked() &&
        			!$('#k4').checked() &&
                    !$('#k5').checked() &&
                    !$('#k6').checked() &&
        			!$('#k7').checked() ){
        		alert("กรุณาระบุสิทธ์กลุ่มนี้อย่างน้อยหนึ่งอย่างค่ะ")
        	}
        	else{
        		String k1 = "null"
        		String k2 = "null"
        		String k3 = "null"
        		String k4 = "null"
        		String k5 = "null"
                String k6 = "null"
                String k7 = "null"
	        	if($('#k1').checked()){	k1 = $('#k1').text() }
	        	if($('#k2').checked()){	k2 = $('#k2').text() }
	        	if($('#k3').checked()){	k3 = $('#k3').text() }
	        	if($('#k4').checked()){	k4 = $('#k4').text() }
	        	if($('#k5').checked()){	k5 = $('#k5').text() }
                if($('#k6').checked()){ k6 = $('#k6').text() }
                if($('#k7').checked()){ k7 = $('#k7').text() }
	        	def groupsave = new Groupemployee()
	        	groupsave.namegroup = $('#namegroup').text()
	        	groupsave.k1 = k1
	        	groupsave.k2 = k2
	        	groupsave.k3 = k3
	        	groupsave.k4 = k4
	        	groupsave.k5 = k5
                groupsave.k6 = k6
                groupsave.k7 = k7
	        	groupsave.save()
	        	redirect(uri: "/employee.zul")
        	}
        })
    }
}