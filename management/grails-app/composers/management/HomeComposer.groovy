package management
import java.util.*
import java.lang.*
import org.zkoss.zk.ui.Component
import org.zkoss.zul.*
import org.zkoss.zk.ui.event.*

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

import java.io.FileOutputStream

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.Image;
import com.itextpdf.text.Chunk;

import javax.servlet.http.*;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.RequestDispatcher;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.ServletException;  



class HomeComposer extends zk.grails.Composer {

    Double sumallm = 0
    Double tempsumall = 0
    float disbath = 0
    boolean checkprin = false
    Double summ = 0
    Double ede = 0

    def afterCompose = { window ->
        //=============================== check session =======================
       def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k7 == session.k7){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================
    
        $('#table').focus()

        int tabl = Integer.parseInt($('#table').text())

         $('#table').on('click',{
            $('#table').select()
        })


         $('#table').on('change',{
            tabl = Integer.parseInt($('#table').text())
        })
         

        $('#search').on('click',{

            
             def tt = Listbill.findByTabl(tabl)
             if(tt == null){
                        alert("ไม่พบข้อมูลครับ")

                    }else{
        
            printrowsell()

            }

            })



        $('#finish').on('click',{
                $('#printbill').setHref("static/ext/pdf2/"+dateandtimeforbill()+".pdf");
                $('#printbill').setTarget("_new");
                 createpdf();
                    alert("บันทึกข้อมูลเรียบร้อยกรุณากดพิมพ์")

                    for(Listbill temph : Listbill.findAll()){
                    if(temph.emid == session.user && tabl == temph.tabl){
                        def savebill = new Final()
                        savebill.usersale = session.user
                        savebill.date = dateandtime()
                        savebill.tabl = temph.tabl
                        savebill.emid = temph.emid
                        savebill.cus = temph.cus
                        savebill.barcode = temph.barcode
                        savebill.nameproduct = temph.nameproduct
                        savebill.nameunit = temph.nameunit
                        savebill.countpro = temph.countpro
                        savebill.priceperunit = temph.priceperunit
                        savebill.sum = temph.sum

                        savebill.total = $('#sumprice').val()
                        savebill.save()
                        
                    }
                }

                    for(Listbill temp : Listbill.findAll()){
                    if(session.user == temp.emid && tabl == temp.tabl){
                        temp.delete()
                    }
                }


        })


        $('#printbill').on('click',{
           

                 def del = Stamplogin.findByUser(session.user)
                if(del != null){
                    session.user = ""
                    session.groupname = ""
                    session.k1 = ""
                    session.k2 = ""
                    session.k3 = ""
                    session.k4 = ""
                    session.k5 = ""
                    session.k6 = ""
                    session.k7 = ""
                    del.delete()
                    redirect(uri: "/login.zul")
                }

        }) 
}

    public String dateandtime(){
        Date date = new Date()
        java.text.SimpleDateFormat df= new java.text.SimpleDateFormat();
        df.applyPattern("dd/MM/yyyy HH:mm:ss")
        return df.format(date)
    }

    public String dateandtimeforbill(){
        Date date = new Date()
        java.text.SimpleDateFormat df= new java.text.SimpleDateFormat();
        df.applyPattern("dd-MM-yyyy-HH-mm-ss")
        return df.format(date)
    }

    public String GetString(String str) {          
            return str
    } 

//เริ่ม

public void createpdf(){
    try {
        Rectangle pagesize = new Rectangle(210f, 360f)
        Document document = new Document(pagesize, 3f, 3f, 3f, 3f)
        PdfWriter.getInstance(document, new FileOutputStream(application.getRealPath("/ext/pdf2/"+dateandtimeforbill()+".pdf")));
        document.open();
        addMetaData(document);
        addTitlePage(document);
        document.close();
    }   catch(Exception e){
            print(e)
            alert("เกิดข้อผิดพลาดในการสร้างใบเสร็จ กรุณากรอกข้อมูลร้านให้ครบ")
    }
}

private void addMetaData(Document document) {
    document.addTitle("My first PDF");
    document.addSubject("Using iText");
    document.addKeywords("Java, PDF, iText");
    document.addAuthor("Lars Vogel");
    document.addCreator("Lars Vogel");
}

private void addTitlePage(Document document) throws DocumentException {
    BaseFont basefont;
    String compa
    String addresscompa
    String num
    byte[] bytes 
    basefont = BaseFont.createFont(application.getRealPath("ARIALUNI.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
    Font catFont = new Font(basefont, 10,Font.NORMAL);
    Font redFont = new Font(basefont, 8,Font.NORMAL, BaseColor.RED);
    Font smallBold = new Font(basefont, 7,Font.BOLD);
    Font smallNomal = new Font(basefont, 8,Font.NORMAL);
    LineSeparator line = new LineSeparator();

    for(Infosetting checkdata : Infosetting.findAll()){
                if(checkdata != null){
                    compa = checkdata.namecompany
                    addresscompa = checkdata.addresscompany
                    num = checkdata.numvat
                    bytes = checkdata.picture
                }
            }

    Image img = Image.getInstance(bytes)
    img.setAlignment(Element.ALIGN_CENTER)
    img.scaleToFit(50,50)
    document.add(img)
    Paragraph preface = new Paragraph();
    preface.add(new Paragraph("ชื่อร้าน : "+compa, catFont));
    preface.add(new Paragraph("ที่อยู่ : "+addresscompa, smallNomal));
    preface.add(new Paragraph("เลขประจำตัวผู้เสียภาษี: "+num, smallNomal));
    preface.add(new Paragraph("วันที่ : "+ new Date(), smallNomal));
    preface.add(line);
    addEmptyLine(preface, 1);
    Paragraph menu = new Paragraph("รายการที่สั่ง"+""+"โต๊ะ : "+$('#table').text(), smallBold);
    menu.setAlignment(Element.ALIGN_CENTER);
    preface.add(menu);
    addEmptyLine(preface, 1);

    
    PdfPTable table = new PdfPTable(4);
    PdfPCell c1 = new PdfPCell(new Phrase("รายการ", smallBold));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    c1 = new PdfPCell(new Phrase("จำนวน", smallBold));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    c1 = new PdfPCell(new Phrase("ราคา/หน่วย", smallBold));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);
    
    c1 = new PdfPCell(new Phrase("รวม", smallBold));
    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(c1);

    for(Listbill temp : Listbill.findAll()){
                    if(temp.tabl == $('#table').val()){
                    table.addCell(new Paragraph("${temp.nameproduct}", smallBold));
                    table.addCell(new Paragraph("${temp.countpro}"+" "+temp.nameunit, smallBold));
                    table.addCell(new Paragraph("${temp.priceperunit}", smallBold));
                    table.addCell(new Paragraph("${temp.sum}", smallBold));
                    summ = summ+temp.sum
                    ede = summ
                }
            }


    document.add(preface);
    document.add(table);

    Paragraph faces = new Paragraph();
    faces.add(new Paragraph("ราคารวม : "+ede + "บาท", smallNomal));  
    document.add(faces);
    
    Paragraph facew = new Paragraph();
    facew.add(new Paragraph("ส่วนลด : "+disbath + "บาท", smallNomal));  
    document.add(facew);

    Paragraph face = new Paragraph();
    face.add(new Paragraph("ราคาสุทธิ : "+(ede - disbath) + "บาท", smallNomal));  
    document.add(face);

    Paragraph footer = new Paragraph("******* ขอบคุณครับที่มาอุดหนุน *******",smallBold)
    footer.setAlignment(Element.ALIGN_CENTER)
    document.add(footer)

document.newPage();


}

private void addEmptyLine(Paragraph paragraph, int number) {
    for (int i = 0; i < number; i++) {
        paragraph.add(new Paragraph(" "));
    }
}
//จบ

    public BigDecimal twodigit(Double num){
        BigDecimal twodi = new BigDecimal(num)
        twodi = twodi.setScale(2,BigDecimal.ROUND_HALF_EVEN)
        return twodi
    }

    
public void printrowsell(){
        Double sumallm = 0
        int j = 1
        int checkbutton = 0
    
 if(checkprin == true){
            $('#listbuy > rows > row').detach()
            checkprin = false
        }

for(Listbill g : Listbill.findAll()){

        if(g.tabl == $('#table').val()){
                        $('#listbuy > rows').append{
                            row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
                                label(value:g.nameproduct,style:"font-size:12px;font-weight:bold;color:black")
                                label(value:g.nameunit,style:"font-size:12px;font-weight:bold;color:black")
                                label(value:g.countpro,style:"font-size:12px;font-weight:bold;color:black")
                                label(value:g.priceperunit,style:"font-size:12px;font-weight:bold;color:black")
                                label(value:g.sum,style:"font-size:12px;font-weight:bold;color:black")
                                sumallm = sumallm+g.sum
                                tempsumall = sumallm
                                button(id:g.id,mold:"trendy",image:"/ext/images/icon/delete_recycle_bin.png",style:"font-size:12px;font-weight:bold;color:black")
                            checkprin = true
                            }

                        }


                    $('#listbuy > rows > row > button').on('click',{
                        for(Listbill finddelete : Listbill.findAll()){
                            if(g.tabl == finddelete.tabl && g.barcode == finddelete.barcode ){
                                def finddelet = Listbill.findById(it.target.id)
                                if(finddelet != null){
                                      finddelet.delete()
                                }
                                redirect(uri: "/home.zul")
                            }
                        }
                        
                    })


                        for(Promotion f : Promotion.findAll()){
                            Date d = new Date()
                            DateFormat  df = new SimpleDateFormat()
                            df.applyPattern("dd/MM/yyyy")
                            Date today = df.parse(df.format(d))
                            Date extcard = df.parse(df.format(f.extdaypro))
                            if(today <= extcard && tempsumall >= Double.parseDouble(f.bathpro)){
                                float percen = Float.parseFloat(f.dispro)
                                disbath = tempsumall*(percen/100) 
                            }
                        }
                        $('#downpricefromlist').val(disbath) 
                        $('#sumpricefromlist').val(tempsumall)
                        $('#sumprice').val(tempsumall-disbath)



            checkprin = true
            
        } 
        
        }
    }

}

