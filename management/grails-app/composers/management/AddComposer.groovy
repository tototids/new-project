package management


class AddComposer extends zk.grails.Composer {

    def afterCompose = { window ->
    	//=============================== check session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k1 == session.k1){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================

        $('#namemodestock').focus()

    	int countlist = 1
    	for(Add m : Add.findAll()){
    		$('#listnamemode > rows').append{
    			row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
    				label(value:"${countlist}",style:"font-size:14px;font-weight:bold;color:black")
    				label(value:m.namemode,style:"font-size:14px;font-weight:bold;color:black")
    				button(id:m.namemode,style:"font-size:14px;font-weight:bold;color:black",image:"/ext/images/icon/delete_recycle_bin.png",mold:"trendy")
    			}
    		}
            countlist++
	    	$('#listnamemode > rows > row > button').on('click',{
	        	def delete = Add.findByNamemode(it.target.id)
	        	delete.delete()
	        	redirect(uri: "/add.zul")
	        })
    	}
    
        $d('#b').on('click'){ ev ->
            redirect(uri: "/newadd.zul")
        }
        
        $('#clear').on('click',{
        	$('#namemodestock').val("")
        })

        $('#submit').on('click',{
        	if($('#namemodestock').text() == ""){
        		alert("กรุณากรอกชื่อหมวดสินค้าด้วยครับ")
        	}
        	else{
        		Add savemode = new Add()
	        	savemode.namemode = $('#namemodestock').text()
	        	savemode.save()
	        	redirect(uri: "/add.zul")
        	}
        })
    }
}
