package management
import org.zkoss.zk.grails.composer.*
import org.zkoss.zk.ui.Executions 
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zul.ListModel;

class NewaddComposer extends zk.grails.Composer{

    def afterCompose = { window ->
    	//=============================== check session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k1 == session.k1){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================


        $d('#b').on('click'){ ev ->
            redirect(uri: "/add.zul")
        }
        
        $('#cancle').on('click',{
        	redirect(uri: "/newadd.zul")
        })

        for(Add m : Add.findAll()){
        	$('#listmode').append{
        		comboitem(label:m.namemode)
        	}
        	$('#listmode2').append{
        		comboitem(label:m.namemode)
        	}
        }

        $('#save').on('click',{
        	if($('#barcod').text() == ""||
        		$('#nameproduct').text() == ""||
        		$('#txtnameunit').text() == ""||
        		$('#pricetoon').text() == ""||
        		$('#listmode').text() == ""
        		){
        		alert("กรุณากรอกข้อมูลให้ครบครับ")
        	}
        	else{
        		Newadd save = new Newadd()
	        	save.barcode = $('#barcod').text()
	        	save.nameproduct = $('#nameproduct').text()
	        	save.nameunit = $('#txtnameunit').text()
	        	save.pricetoon = Double.parseDouble($('#pricetoon').text())
	        	save.modeproduct = $('#listmode').text()
	        	save.noteproduct = $('#note').text()
	        	save.save()
	        	redirect(uri: "/newadd.zul")
        	}
        })

		boolean checkedit = false
       	boolean checkprintbeforchecedit = false
       	boolean checkprint = false

    $('#listmode2').on('select',{	
     	if(checkprint == true){
				$('#listproduct > rows > row').detach()
				checkprint = false
			}	
			for(Newadd m : Newadd.findAll()){
				if(m.modeproduct == $('#listmode2').text()){
					$('#listproduct > rows').append{
						row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
							label(value:""+m.barcode,style:"font-size:14px;font-weight:bold;color:black")
			                label(value:""+m.nameproduct,style:"font-size:14px;font-weight:bold;color:black")
			                label(value:""+m.modeproduct,style:"font-size:14px;font-weight:bold;color:black")
			                label(value:""+m.pricetoon,style:"font-size:14px;font-weight:bold;color:black")
			                label(value:""+m.nameunit,style:"font-size:14px;font-weight:bold;color:black")
			                label(value:""+m.noteproduct,style:"font-size:14px;font-weight:bold;color:black")
							if(checkedit == false){
									button(orient:"vertical",mold:"trendy",image:"/ext/images/icon/delete_recycle_bin.png",id:m.barcode)
									checkprintbeforchecedit = true
							}

						}
					}
					$('button').on('click',{
						def finddelete = Newadd.findByBarcode(it.target.id)
						if(finddelete != null){
							finddelete.delete()
						}
						redirect(uri: "/newadd.zul")
					})
					checkprint = true
				}
			}
	})

	}

}

    
    

