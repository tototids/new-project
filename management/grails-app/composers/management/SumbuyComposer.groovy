package management


class SumbuyComposer extends zk.grails.Composer {

    def afterCompose = { window ->
         //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k4 == session.k4){
                window.visible = true
            }
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================

        String stringstate = ""
        for(Buy m : Buy.findAll()){
            if(stringstate != m.productimport.substring(3)){
                stringstate = m.productimport.substring(3)
                $('#search').append{
                    comboitem(label:m.productimport.substring(3))
                }
            }
        }

        Double sumall = 0
        $('#search').on('select',{
            for(Buy b : Buy.findAll()){
                if($('#search').text() == b.productimport.substring(3)){
                    $('#listbuy > rows').append{
                        row(style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
                            label(value:b.productimport,style:"font-size:14px;font-weight:bold;color:black")
                            label(value:b.barcode,style:"font-size:14px;font-weight:bold;color:black")
                            label(value:b.nameproduct,style:"font-size:14px;font-weight:bold;color:black")
                            label(value:b.modeproduct,style:"font-size:14px;font-weight:bold;color:black")
                            label(value:b.pricetoon,style:"font-size:14px;font-weight:bold;color:black")
                            label(value:b.countproduct,style:"font-size:14px;font-weight:bold;color:black")
                            label(value:b.pricetoon*b.countproduct,style:"font-size:14px;font-weight:bold;color:black")
                            sumall = sumall+(b.pricetoon*b.countproduct)
                            label(value:"บาท",style:"font-size:14px;font-weight:bold;color:black")
                        }
                    }
                }
            }

            $('#listbuy > rows').append{
                row(spans:"6",style:"font-size:14px;font-weight:bold;color:black;background:pink;"){
                    label(value:"รวม",style:"font-size:14px;font-weight:bold;color:red")
                    label(value:"${sumall}",style:"font-size:14px;font-weight:bold;color:red")
                    label(value:"บาท",style:"font-size:14px;font-weight:bold;color:red")
                }
            }
        })

    }
}
