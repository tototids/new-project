package management

import java.io.*
import org.zkoss.io.*
import org.zkoss.util.media.Media
import org.zkoss.zhtml.Fileupload
import org.zkoss.zk.ui.event.EventListener
import org.zkoss.zk.ui.event.UploadEvent
import org.zkoss.image.Image
import org.zkoss.image.AImage


class SettingComposer extends zk.grails.Composer {

    def afterCompose = { window ->

        Media media
        Image img

        //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k6 == session.k6){
                window.visible = true
            } 
            else{
                redirect(uri: "/blank.zul")
            }
        }
        //=====================================================================
        
        $d('#card').on('click'){ ev ->
            redirect(uri: "/promotion.zul")
        }

        $('#upload').on('upload'){ ev->
            media = ev.getMedia()
            img = (Image) media
            $('#imageupload').setContent(img)
        }

        for(Infosetting checkdata : Infosetting.findAll()){
	      	if(checkdata != null){
				$('#company').val(checkdata.namecompany)
				$('#address').val(checkdata.addresscompany)
                $('#vatnum').val(checkdata.numvat)
                byte[] bytes = checkdata.picture
                $('#imageupload').setContent(new AImage("amphib", bytes))
		    }
		}

        $('#save').on('click',{
            Boolean checkstate = false
            for(Infosetting checkdata : Infosetting.findAll()){
                if(checkdata != null){
                    checkdata.namecompany = $('#company').text()
                    checkdata.addresscompany = $('#address').text()
                    checkdata.numvat = $('#vatnum').text()
                    checkdata.picture = $('#imageupload')[0].getContent().getByteData()
                    checkdata.save()
                    checkstate = true
                    alert("บันทึกข้อมุลเรียบร้อยครับ")
                }
            }
            if(checkstate == false){
                def saveinfo = new Infosetting()
                saveinfo.namecompany = $('#company').text()
                saveinfo.addresscompany = $('#address').text()
                saveinfo.numvat = $('#vatnum').text()
                saveinfo.picture = $('#imageupload')[0].getContent().getByteData()
                saveinfo.save()
                alert("บันทึกข้อมุลเรียบร้อยครับ")
            }
        })

    $('#save').on('click',{
        redirect(uri: "/index.zul")
    })

     $('#clear').on('click',{
        redirect(uri: "/setting.zul")
    })
     
    }
}