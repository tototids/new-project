package management

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date


class PromotionComposer extends zk.grails.Composer {

    def afterCompose = { window ->

    	 //=============================== chec session =======================
        def checkgroup = Groupemployee.findByNamegroup(session.groupname)
        if(checkgroup != null){
            if(checkgroup.k6 == session.k6){
                window.visible = true
            }
            else{
                redirect(uri: "/index.zul")
            }
        }
        //=====================================================================
       
           Date date = new Date()
        	for(Promotion checkpro : Promotion.findAll()){
            $('#extdaypro').setValue(checkpro.extdaypro)
            $('#bathpro').val(Integer.parseInt(checkpro.bathpro))
            $('#dispro').val(Integer.parseInt(checkpro.dispro))
            $('#savepro').setDisabled(true)
        }

			$('#clearpro').on('click',{
            for(Promotion checkpro : Promotion.findAll()){
                checkpro.delete()
                redirect(uri: "/promotion.zul")
            }
        })



        $('#savepro').on('click',{

            if($('#extdaypro').text() == "" ||
                $('#bathpro').text() == "" ||
                $('#dispro').text() == "" 
                ){
                alert("กรุณากรอกข้อมูลให้ครบครับ")
            }
            else{ 
                Boolean checkstate = false
                for(Promotion checkpro : Promotion.findAll()){
                    if(checkpro != null){
                        checkpro.extdaypro = $('#extdaypro').getValue()
                        checkpro.bathpro = $('#bathpro').text()
                        checkpro.dispro = $('#dispro').text()
                        checkpro.save()
                        print(checkpro.errors)
                        checkstate = true
                        alert("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
                        redirect(uri: "/promotion.zul")
                    }
                }
                if(checkstate == false){
                    def promo = new Promotion()
                    promo.extdaypro = $('#extdaypro').getValue()
                    promo.bathpro = $('#bathpro').text()
                    promo.dispro = $('#dispro').text()
                    promo.save()
                    print(promo.errors)
                    alert("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
                    redirect(uri: "/promotion.zul")
                }
            }  
        })


	$d('#setting').on('click'){ ev ->
        redirect(uri: "/setting.zul")
    }
    
    }
}
